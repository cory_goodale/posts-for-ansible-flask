- convert over to using SQLAlchemy instead of direct DB calls.
- authenticate blog users via db users vs hardcoded
- authorize users to various parts
  - admin users who are able to edit/delete any blog post
  - only the poster can edit/delete a blog post
  - only logged in users can create blog posts
- add a wysiwyg editor for creation of block post
  - protect against inline js
- add validation to blog creation screen
- add slug urls for blog posts for seo (i.e. http://localhost:5000/blog/ansible-flask)
- only show first section of blog posts, w/ click through to full post
- add ability to upload/host images in blogs
- add an rss feed
- add category tagging to blog entries
- refactor for flask blueprints
- add ability to edit blog posts
- add email validation on signup
  - i.e. create user in unverified state and then activate once clicking through url in email.
- integrate with mandrill/sendgrid for email sending
- allow users to follow blog posters
  - homepage should pull recent blog posts from bloggers they follow
  - not following anyone, show most recent editor choices
- ability to see posts for a single blogger
  - i.e. /b/brian/ would be a single blog site
- add blogger profile page
  - name, photo, birthday, hometown (on map), bitbucket link, facebook link, github link, about me
