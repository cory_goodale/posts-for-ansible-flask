# -*- coding: utf-8 -*-
"""
    changeme.users
    ~~~~~~~~~~~~~~

    changeme users package
"""

from ..core import Service
from .models import User


class UsersService(Service):
    __model__ = User
