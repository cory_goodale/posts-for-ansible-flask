#!/bin/sh

if [ $# -eq 1 ]; then
    ansible-playbook -i ansible/hosts ansible/deploy.yml --limit=$1
else
    echo 'ERROR: please specify "vagrant" or "production"'
fi
