# -*- coding: utf-8 -*-
"""
    tests.frontend
    ~~~~~~~~~~~~~~

    frontend tests package
"""

from changeme.frontend import create_app

from .. import ChangemeAppTestCase, settings


class ChangemeFrontendTestCase(ChangemeAppTestCase):

    def _create_app(self):
        return create_app(settings)

    def setUp(self):
        super(ChangemeFrontendTestCase, self).setUp()
        self._login()
