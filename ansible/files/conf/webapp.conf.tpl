# simple uWSGI script

description "uwsgi tiny instance"
start on runlevel [2345]
stop on runlevel [06]

exec /usr/local/bin/uwsgi --master --processes 4 --threads 2 --die-on-term --socket :3031 --chdir {{ project_root }}/code --pp  {{ project_root }}/code --virtualenv  {{ project_home }}/.virtualenvs/{{ project_name }} --module wsgi:application --stats :9191
