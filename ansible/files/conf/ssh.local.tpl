Host changeme.*
    User changeme
    IdentityFile ~/.ssh/conf.d/changeme/id_rsa

Host changeme.local
    Hostname 127.0.0.1
    Port 2222
