# Ansible-Flask

**NOTE** All instructions are run locally unless otherwise specified
**NOTE** On Windows make sure VT-X is enabled in the Bios or you will not be able to bring up virtual machines with VirtualBox

This is a base setup to get a flask application up and running using Ansible and Vagrant.  This sample uses **changeme** throughout.  You will want to find an update them before really starting your project.  To locate them you can run:

    $ find . -type f | xargs grep changeme

The Vagrant configuration is set up to mount your local code directory into the virtual machine.  This means that you can continue to develop in your favorite editor or IDE on your local machine, while your server runs isolated in the virtual machine.  All server changes should be made through the ansible configuration.  This allows for you to blow away your virtual machine and start fresh when needed.  I encourage you to to establish a weekly ritual where you start with a fresh virtual machine, built from scratch, to keep you honest.  This is so important and I will say it again.  You are probably not thinking about putting this onto a hosted server at this point in time and it will be a huge pain if you don't keep track of everything needed to run your system.

## Local Setup - OS X

### Install Homebrew
    $ ruby -e "$(curl -fsSL https://raw.github.com/mxcl/homebrew/go/install)"

### Tap new Homebrew repository
    $ brew tap phinze/homebrew-cask
    $ brew install brew-cask
    $ export HOMEBREW_CASK_OPTS="--appdir=/Applications"

The above export statement should be added to your ~/.bash_profile file

### Install Vagrant
    $ brew cask install vagrant

### Install VirtualBox
    $ brew cask install virtualbox

### Install Git-Encrypt
    $ brew install git-encrypt

### Install Pip
    $ sudo easy_install pip

### Install Ansible
    $ sudo ARCHFLAGS=-Wno-error=unused-command-line-argument-hard-error-in-future pip install ansible

## Local Setup - Linux

### Install Vagrant
    $ apt-get install vagrant

### Install VirtualBox
    $ apt-get install virtualbox

### Install Git-Encrypt
Clone git-encrypt somewhere on your local machine:

    $ git clone https://github.com/shadowhand/git-encrypt
    $ cd git-encrypt

The gitcrypt command must be executable:

    $ chmod 0755 gitcrypt

And it must be accessible in your $PATH:

    $ sudo ln -s "$(pwd)/gitcrypt" /usr/local/bin/gitcrypt

### Install Pip
    $ apt-get install python-pip

### Install Ansible
    $ sudo pip install ansible

## Local Setup - Windows
Cygwin should be used for all commandline steps

### Install Cygwin
    http://www.cygwin.com/install.html

Some additional packages selected for installation:

* python
* python-paramiko
* python-setuptools
* python-crypto
* git
* vim
* openssh
* openssl

Now we need to manually install some extra packages which are not available in Cygwin repositories

* PyYAML 3.10 https://pypi.python.org/pypi/PyYAML/3.10
* Jinja2 2.6 https://pypi.python.org/pypi/Jinja2/2.6

Download and unpack .tar.gz files to some Cygwin temporary directory. Then install each package with comamnd:

    $ python setup.py install

### Install Ansible
    $ git clone https://github.com/ansible/ansible /opt/ansible

In .bash_profile of Cygwin user - add necessary variables:

/home/<User>/.bash_profile

    if [ -f "${HOME}/.bashrc" ] ; then
      source "${HOME}/.bashrc"
    fi

    ANSIBLE=/opt/ansible
    PATH=$PATH:$ANSIBLE/bin
    export PATH
    PYTHONPATH=$ANSIBLE/lib
    export PYTHONPATH
    ANSIBLE_LIBRARY=$ANSIBLE/library
    export ANSIBLE_LIBRARY

### Install Vagrant
    http://downloads.vagrantup.com/

### Install VirtualBox
    https://www.virtualbox.org/wiki/Downloads

### Install Git-Encrypt
    https://github.com/shadowhand/git-encrypt

## Getting Things Running

### Clone the project and set up git-encrypt:

    $ git clone -n https://bitbucket.org/brianmorgan/ansible-flask.git
    $ cd ansible-flask
    $ gitcrypt init

The values are value values to provide to init

    salt:   da983b175e6828bd
    pass:   changeme
    cipher: aes-256-ecb

    Do you want to use .git/info/attributes? Y
    What files do you want encrypted? *.encrypted

You should **not** store this information in the same repository, it is here as a example.  You can read more about this here:

    https://github.com/shadowhand/git-encrypt

At this point there is no code, run the following command

    $ git reset --hard HEAD

You will notice that you can read the contents of the private key

    ansible/files/ssl/changeme_id_rsa.encrypted

If you attempt to look at this file in a repository without git-encrypt setup it will be encrypted and not readable.  This is a great way to store sensitive information in a hosted and/or public repository.

### Add the following to your ~/.ssh/config file

    HostName 127.0.0.1
      User vagrant
      Port 2222
      UserKnownHostsFile /dev/null
      StrictHostKeyChecking no
      PasswordAuthentication no
      IdentityFile ~/.vagrant.d/insecure_private_key
      IdentitiesOnly yes
      LogLevel FATAL

### Run Server

    $ vagrant up
    $ ./provision.sh vagrant

### Test the Server

    $ open http://localhost:8080/changeme

This is the production server being served up from uswgi via nginx.  You will typically not use this during development, but it is available for testing prior to releasing.  This is the EXACT configuration that will be running on your production machine.

## Developing

### Create your own private/public key pairs

    $ ssh-keygen -t rsa

Both of these files should be stored in

    ansible-flask/ansible/files/ssl/

You should override the files that are there, when you change "changeme" in the vars.yml file, the name of these files will also need to be updated.  **Note** that the private key ends in .encrypted.  See the Git-Crypt section for details.

Copy the private key to your ssh folder

    $ cp ansible-flask/ansible/files/ssl/changeme_id_rsa.encrypted ~/.ssh/changeme_id_rsa
    $ chmod 700 ~/.ssh/changeme_id_rsa

### Add the following to your ~/.ssh/config file

    Host changeme.*
        User changeme
        IdentityFile ~/.ssh/conf.d/changeme/id_rsa

    Host changeme.local
        Hostname 127.0.0.1
        Port 2222

    Host changeme.production
        Hostname changeme.com

**NOTE** ~/.vagrant.d/insecure_private_key must be replaced with C:/Users/<username>/.vagrant.d/insecure_private_key on windows

At this point (if you have already run vagrant up) you should be able to run

    $ ssh changeme.local
    $ pwd

You should be in /home/changeme on your local virtual machine

### Running a Development Server

    $ ssh changeme.local
    $ workon changeme
    $ python wsgi.py

The *workon* command will put you into a virtualenv used to keep your python libraries separate from the system or other projects.  This server is set up to auto-reload and as you make changes to the files (i.e. wsgi.py) it will reload and you should see the changes on the next page refresh.

### Test the Dev Server

    $ open http://localhost:5000/
    $ open http://localhost:5000/changeme

# Deploying Ansible Changes

Running vagrant up not only creates the virtual machine, but also runs the ansible provision playbook.  The provision playbook also includes a deployment playbook that installs the software, their dependencies, and makes sure the server is running.

If you make a change to provision.yml you can run the following:

    ./provision.sh [vagrant|production]

If you make changes to only deploy.yml you can run the following:

    ./deploy.sh [vagrant|production]

Before you can deploy to production you will need update ansible/hosts with the production server information.  You will also need to install the changeme_id_rsa.pub onto the production server before you can provisioning or deployment against it.

# Resources

* **Vagrant** (http://www.vagrantup.com/) -- Awesome virtual machine wrapper around **VirtualBox** (https://www.virtualbox.org/).  This is my new favorite tool.  It allows you to develop on your local box in the same environment as your production server.  I would tend towards using Ubuntu 12.04.2 LTS.  It is available on virtualbox and amazon, so you can have exact  environments
* **Ansible** (http://www.ansibleworks.com/) -- Great tool for provisioning and deployment to both local virtual machines and production servers.
* For Web Frame work, I have used **Pyramid** in the past (http://www.pylonsproject.org/).  I used its predecessor (Pylons) at OpenMile.  Starting from scratch, I would take a look at **Flask** (http://flask.pocoo.org/).  I like how lightweight the core framework is, but there is a lot of addons to do pretty much everything.  Being so minimal also makes it a great API server.
* **Celery** (http://www.celeryproject.org/) -- This is a task/job management framework.  You will definitely need one to handle one off background processes, as well as, things that need to happen on a repeated schedule.
* **MySQL** (http://www.mysql.com/) -- I have always use mysql as my database of choice, but people love **PostgreSQL** (http://www.postgresql.org/).  I have never had the time to sit down and figure out the differences and the gotchas.
* **SQLAlchemy** (http://www.sqlalchemy.org/)  -- One of the best ORMs out there, and by far the best for Python.  This will give you your database abstraction in your MVC or MVP model.  I would encourage you to look at MVP, which usually isn't talked about much in interpretive languages, but makes your app much more testable by reducing the amount of logic in your controller.
* **Nginx** (http://wiki.nginx.org/Main) -- Reverse proxy server.  You are not going to want to expose your python servers directly to your end users.  You will want to put a reverse proxy in place to act as a load balancer and something that can server up static content efficiently.  You could also use apache (http://httpd.apache.org/), but nginx tends to be more performant and my server of choice.
* **uwsgi** (http://projects.unbit.it/uwsgi/) -- this is the python server you will want to use in production.  Both Pyramid and Flask come with their own servers and you can use them behind nginx, but I prefer to use uwsgi instead in production.
* There are tons of web technologies you should consider using as well.  **Bootstrap** (http://getbootstrap.com/) is the best component base html util that makes building beautiful sites trivial.  **LessCSS** (http://lesscss.org/) is a meta CSS language that makes writing CSS easier.  **CoffeeScript** (http://coffeescript.org/) is a meta JavaScript language that makes it feel a bit more like Python (a personal choice, but something I like).  **JQuery** (http://jquery.com/) is a set if library functions that make using javascript with DOM elements trivial.  There are a ton more, but depends on your application :-)

